class SuperLuquinhas extends Phaser.Game
{
    constructor()
    {
        super(500, 700, Phaser.AUTO, 'content');
        this.state.add('main', GameState, true);
    }
}

class GameState extends Phaser.State
{
    keys: Phaser.CursorKeys;
    platforms: Phaser.Group;
    player: Phaser.Sprite;
    cameraCeil: number = 9999;
    platformCeil: number = 9999;
    playerStart: number = 0;
    playerDelta: number = 0;
    firstPlatform: Phaser.Sprite;

    create()
    {
        this.keys = this.game.input.keyboard.createCursorKeys();
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.stage.backgroundColor = '#007236';

        var bg = this.game.add.image(0, 0, 'background');
        bg.fixedToCamera = true;

        this.setupPlatforms();
        this.setupPlayer();
    }

    preload()
	{
        this.game.load.spritesheet('player', 'assets/player.png', 128, 176, 3);
        this.game.load.image('platform', 'assets/carajo.png');
        this.game.load.image('base', 'assets/base.png');
        this.game.load.image('background', 'assets/bg.jpg');
    }

    reset()
    {
        this.platforms.destroy();
        this.player.destroy();
        this.cameraCeil = 0;
        this.platformCeil = 0;
        this.playerStart = 0;
        this.playerDelta = 0;
        this.world.setBounds(0, 0, this.game.width, this.game.height);

        this.setupPlatforms();
        this.setupPlayer();
    }

    setupPlatforms()
    {
        this.platforms = this.game.add.group();
        this.platforms.enableBody = true;
        this.platforms.createMultiple(8, 'platform');

        this.firstPlatform = this.createPlatform(-25, this.game.world.height - 10, this.game.world.width + 30, 'base');

        for (var i: number = 0; i < 7; i++) {
            this.createPlatform(this.game.rnd.integerInRange(0, this.game.world.width - 80), this.game.world.height - 180 * i);
        }
    }

    setupPlayer()
    {
        this.player = this.game.add.sprite(this.game.world.centerX, this.game.world.height - 190, 'player', 2);
        this.player.animations.add('jump', [2, 1, 0], 10, false);
        this.player.animations.add('air', [0], 10, false);
        this.player.animations.add('standing', [2], 10, false);

        this.playerStart = this.player.y;
        this.playerDelta = 0;

        this.game.physics.arcade.enable(this.player);
        this.player.body.checkCollision = {none: false, any: true, up: false, down: true, left: false, right: false};
        this.player.body.gravity.y = 512;
    }

    createPlatform(x: number, y: number, width?: number, key?: string): Phaser.Sprite
    {
        var platform: Phaser.Sprite = this.platforms.getFirstDead();

        if (key) {
            platform.loadTexture(key);
        }

        platform.reset(x, y);
        platform.body.immovable = true;

        if (width) {
            platform.width = width;
            platform.height = 16;
        }

        return platform;
    }

    update()
    {
        this.game.world.setBounds(0, -this.playerDelta, this.game.world.width, this.game.height + this.playerDelta);
        this.cameraCeil = Math.min(this.cameraCeil, this.player.y - this.game.height + 300);
        this.game.camera.y = this.cameraCeil;

        this.updatePlayer();
        this.updatePlatforms();
    }

    updatePlayer()
    {
        this.game.physics.arcade.collide(this.player, this.platforms);

        if (this.keys.left.isDown) {
            this.player.body.velocity.x = -180;
        } else if (this.keys.right.isDown) {
            this.player.body.velocity.x = 180;
        } else {
            this.player.body.velocity.x = 0;
        }

        if (this.firstPlatform && this.keys.up.isDown) {
            this.platforms.remove(this.firstPlatform, true);
            this.firstPlatform = null;
        }

        if (this.player.body.touching.down && !this.firstPlatform) {
            this.player.animations.play('jump');
            this.player.body.velocity.y = -500;
        }

        this.playerDelta = Math.max(this.playerDelta, Math.abs(this.player.y - this.playerStart));
        this.game.world.wrap(this.player, 70);

        if (this.player.y > this.cameraCeil + this.game.height && this.player.alive) {
            this.reset();
        }
    }

    updatePlatforms()
    {
        this.platforms.forEachAlive((platform) => {
            this.platformCeil = Math.min(this.platformCeil, platform.y)

            if (platform.y > this.game.camera.y + this.game.height) {
                platform.kill();
                this.createPlatform(this.game.rnd.integerInRange(0, this.game.world.width), this.platformCeil - 120);
            }
        }, this);
    }
}

window.onload = () => {
    var game = new SuperLuquinhas();
};